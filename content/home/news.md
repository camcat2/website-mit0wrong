+++
#Custom widget.
#An example of using the custom widget to create your own homepage section.
#To create more sections, duplicate this file and edit the values below as desired.
#widget = "custom" active = true date = 2016-04-20T00:00:00

#Note: a full width section format can be enabled by commenting out the title and subtitle with a #.
title = "News"
subtitle = ""
active = false

#Order that this section will appear in.
weight = 60

+++

* **February**
  * The preprint of my latest article on theoretical and numerical models of earthquake statistics on simple planar faults is available on the EarthArXiv: [Complex earthquake behavior on simple faults](https://eartharxiv.org/hgbjx/).


<!---break-->

* **April**
  * Invited seminars at CalTech (April 6) and MIT (April 11).

  * April 23-26: I'll be at the SSA meeting in Seattle, where I'll give a [talk](https://seismosoc.secure-platform.com/a/solicitations/5/sessiongallery/51/application/4143) on theoretical arguments for the statistical properties of seismicity on simple planar faults, and co-chair a [session](https://seismosoc.secure-platform.com/a/solicitations/5/sessiongallery/27) on earthquake forecasting.

[News from 2018](/post/news-2018)

***2018***

<!---break-->

* **February**
  * Friday 26 - Invited talk at the Berkeley Seismology Lab.

  * Congratulations to PhD student Simone Mancini, whose AGU [talk](http://adsabs.harvard.edu/abs/2018AGUFM.S24C..06M) on Coulomb stress models of the 2016-2017 Central Italy sequence won him an Oustanding Student Paper Award!


<!-- break --> 

* **December**
  * I'll be at the AGU meeting in Washington, where I'll give a [talk](https://agu.confex.com/agu/fm18/meetingapp.cgi/Paper/413351) on my recent [paper](https://agupubs.onlinelibrary.wiley.com/doi/pdf/10.1029/2018JB016056) about crack models of small repeating earthquakes. And don't miss other presentations by my coauthors: Simone Mancini will give a [talk](https://agu.confex.com/agu/fm18/meetingapp.cgi/Paper/412682) on Coulomb stress models of the 2016-2017 Central Italy sequence; Eric Burton will present a [poster](https://agu.confex.com/agu/fm18/meetingapp.cgi/Paper/446112) on time dependent fault locking in Japan; Brittany Erickson and colleagues from the Community Code Verification Exercise for Simulating Sequences of Earthquakes and Aseismic Slip will present a [poster](https://agu.confex.com/agu/fm18/meetingapp.cgi/Paper/375175) with the results of the first benchmarks.

<!-- break -->


* **November**
  * Our article on [modeling of small repeating earthquakes](/project/repeaters/) has been accepted in JGR. Check it out [here](https://agupubs.onlinelibrary.wiley.com/doi/10.1029/2018JB016056)!

<!-- break -->

* **August**
  * I will attend the SCEC meeting (September 9-12, Palm Springs), the *Workshop on Modeling Earthquake Source Processes* (October 8-10, Pasadena), and AGU. My coauthors and I will present work on [modeling of small repeating earthquakes](/project/repeaters/), [time dependent fault locking prior to the Tohoku earthquke](/project/tohoku/), [Coulomb stress models of the 2016-2017 Central Italy sequence](/project/coulomb), and a [comparison of numerical codes simulating earthquake sequences](https://www.scec.org/publication/8214). Stay tuned for details!

<!-- break -->

* **July**
  * invited talk at ETH, Zurich (*Crack models to explain seismic cycles at different scales: small repeating earthquakes and vertical strike slip faults*)


<!-- break -->

* **June**
  * our article on [testing Coulomb models for operational earthquake forecasting](https://pubs.geoscienceworld.org/ssa/srl/article-abstract/89/4/1238/532042/the-forecasting-skill-of-physics-based-seismicity?redirectedFrom=PDF) was described in an article in [phys.org](https://phys.org/news/2018-06-international-collaboration-earthquakes.html), reviewing recent progress of the Collaboratory for the Study of Earthquake Predictability.

  * the *Seismological Research Letters* special Focus Section on the Collaboratory for the Study of Earthquake Predictability (CSEP) is out! Check out our study on [testing Coulomb models for operational earthquake forecasting](https://pubs.geoscienceworld.org/ssa/srl/article-abstract/89/4/1238/532042/the-forecasting-skill-of-physics-based-seismicity?redirectedFrom=PDF).

<!-- break -->

<!--* **April**
  * I will attend the [SCEC Workshop on Rupture Dynamics Code Validation and Comparing Simulations of Earthquake Sequences and Aseismic Slip](https://www.scec.org/workshops/2018/cvws-seas), and discuss.-->



