---
title: Fault mechanics and earthquake cycles
summary: How statistical properties of seismicity emerge from fracture mechanics
tags:
#- current
#- earthquake cycle
#- earthquake periodicity 
#- power-law distributions
#- earthquake statistics
#- fault mechanics
#- rate-state friction
#- fracture mechanics
#- earthquake scaling laws

weight: 30

date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: On sufficiently large faults, simulated sequences exhibit a power-law distribution in interevent times, similar to observed catalogs. From *Cattania, 2019*, [*Complex Earthquake Sequences On Simple Faults*](https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019GL083628)
  focal_point: Smart
  placement: 1 
#  preview_only: true

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

While some seismic sources rupture in characteristic, quasi-periodic earthquakes, most faults produce a more irregular pattern, with temporal clustering and a power-law distribution of rupture lengths. Why do we see such different behaviors?

We've been addressing this question from the prospective of fracture mechanics, connecting fault physics at a microscopic scale with the timing and extent of earthquake ruptures. Energy balance criteria predict that the timing and size of seismic events is controlled by the dimension of a fault relative to a characteristic length arising from frictional and elastic properties; for sufficiently large faults, this leads to power-law distributions commonly observed in earthquake catalogs. On the other hand, small faults can rupture in simple, quasi-periodic sequences of identical events. We have depeloped theoretical arguments predicting the recurrence intervals and its scaling with magnitude, in agreement with observations of small repeating earthquakes worldwide.

Until now, we have considered a rather idealized fault geometry and spatial distribution of frictional properties. Current and future work explores more complex and realistic cases, with applications to subduction zone cyles.






