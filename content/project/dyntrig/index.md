---
title: Dynamic triggering on transform faults
summary: We used stastical tools to analyze data from ocean bottom seismometers in the East Pacific Rise, and detect instances of dynamic earthquake triggering from remote mainshocks.
#tags:
##- past
date: "2016-04-27T00:00:00Z"
weight: 100

# Optional external URL for project (replaces project detail page).
external_link: "/publication/cattania-2017-k/"

image:
  caption: 
  focal_point: Smart
  
links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

